# MPD bot
(python)

Control de reproduccion MPD (linux) con telegram.

El bot de uso personal, es decir, no permite que otros usuarios que no seas tú (o quien tú definas), hagan uso de él, debido a que controla TU música.


Módulos python requeridos:

pytelegrambotapi (telebot)

os

subprocess


Además requieres tu id personal de telegram y el TOKEN de un bot (creado con el bot telegram BotFather).

Se requiere tener instalado y configurado previamente MPD en tu equipo
