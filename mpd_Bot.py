#!/usr/bin/python

import telebot
import os
import subprocess

from telebot import types

mi_token = >Token_aqui>
bot = telebot.TeleBot(mi_token)
cid = <TU_id_telegram>

#   -----  Usuario NO permitido
def kick_user(m):

    with open('aviso.txt','r') as avi:
        aviso = avi.read()
        bot.send_message(cid, aviso)
#    avi.close()

#   ----  Verifica el estado inicial de MPD
def mpd_start():

    pid_mpd = int(os.system("ps -aux | grep -e 'mpd' | grep -v -e 'grep' -e 'mpd_Bot.py' "))

    if pid_mpd != 0:
        os.system("mpd")
        os.system("mpc stop")
        bot.send_message(cid, '‼️ MPD apagado\n Se INICIA servicio de música MPD')
        print ("Inicia MPD")

@bot.message_handler(commands=['start', 'help', 'inicio'])
def inicio_mpd(m):

    uid = m.from_user.id

    if cid != uid:
        kick_user(m)
    else:
        mpd_start()

        with open('saludo.txt','r') as salud:
            saludo = salud.read()
            bot.send_message(cid, saludo)

        titulo = subprocess.getoutput("mpc status") #print (pid_mpd)

        markup = types.ReplyKeyboardMarkup(row_width=3, resize_keyboard=True)
        markup.add('/toggle', '/next', '/prev', '/stop', '/status', '/kill_mpd')
        bot.send_message(cid, 'Status inicial 🎵 ' + titulo, reply_markup=markup)

@bot.message_handler(commands=['toggle'])
def play_mpd(m):

    uid = m.from_user.id

    if cid != uid:
        kick_user(m)
    else:
        mpd_start()
        os.system("mpc toggle")
        titulo = subprocess.getoutput("mpc status | head -n1")
        bot.send_message(cid, '▶️ ' + titulo)

@bot.message_handler(commands=['stop'])
def play_mpd(m):

    uid = m.from_user.id

    if cid != uid:
        kick_user(m)
    else:
        mpd_start()
        os.system("mpc stop")
        bot.send_message(cid, '⛔️ Música detenida')

@bot.message_handler(commands=['next'])
def play_mpd(m):

    uid = m.from_user.id

    if cid != uid:
        kick_user(m)
    else:
        mpd_start()
        os.system("mpc next")
        titulo = subprocess.getoutput("mpc status | head -n1")
        bot.send_message(cid, '▶️ ' + titulo)

@bot.message_handler(commands=['prev'])
def play_mpd(m):

    uid = m.from_user.id

    if cid != uid:
        kick_user(m)
    else:
        mpd_start()
        os.system("mpc prev")
        titulo = subprocess.getoutput("mpc status | head -n1")
        bot.send_message(cid, '▶️ ' + titulo)

@bot.message_handler(commands=['status'])
def play_mpd(m):

    uid = m.from_user.id

    if cid != uid:
        kick_user(m)
    else:
        mpd_start()
        titulo = subprocess.getoutput("mpc status")
        bot.send_message(cid, '🌟 ' + titulo)

@bot.message_handler(commands=['kill_mpd'])
def play_mpd(m):
            os.system("mpc stop")
            os.system("killall -9 mpd")


mpd_start()
bot.polling(none_stop=True)
